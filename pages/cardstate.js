var editApps = angular.module("editApps", []);

editApps.controller('editCtrl', function($scope, $http, $window){
    $scope.upButShow = true;
    $scope.huahua = 'huahua';

    $scope.transSearch = function(){
        

        $http.post('model.php?action=get_product',
        {
            'transno' : $scope.upTransNo
        }
        )
        .then(function(response){
            $scope.upUID = response.data[0]['uid'];
            $scope.upNumber = response.data[0]['number'];
            $scope.upDate = response.data[0]['date'];
            $scope.upSellerno = response.data[0]['sellerno'];
            $scope.upProduct = response.data[0]['product'];
            $scope.upPrice = response.data[0]['price'];
        });
    }
    $scope.test = function(){
        alert('aland');
        return 'kalabunga';
    }
    $scope.submit = function(){
        //alert($scope.newPrice);
        //alert('Entering submit');
        $http.post('model.php?action=add_product', 
                {
                    //'name'     : $scope.newName, 
                    'number'     : $scope.newNumber, 
                    'date'    : $scope.newDate,
                    'sellerno' : $scope.newSellerno,
                    'product' : $scope.newProduct,
                    'price' : $scope.newPrice,
                    //'uid' : $scope.newUid
                }
            )
            .success(function (data, status, headers, config) {
              alert("Product has been Submitted Successfully");

              //$scope.myVar = $window._yourSpecialVar
            })

            .error(function(data, status, headers, config){
               
            });
    }
    $scope.inputCheck = function(){
        if(!isNaN($scope.updateUid)){
            return false;
        }
    }
    $scope.update = function(){
        //alert($scope.upTransNo);
        //alert('hello');
        // if(!isNaN($scope.updateUid)){
        //     break;
        // }
        //alert(upUid+updateName+updateNumber+updateIssuer+updateExpiration+updateLimit+updateCurrency+upUid);
        //alert(updateName);
        //alert('update is not implemented yet.');
        $http.post('model.php?action=update_product',
                    {
                        'sellerno' : $scope.upSellerno,
                        'product' : $scope.upProduct,
                        'price' : $scope.upPrice,
                        'transno' : $scope.upTransNo
                    }
                  )
                .success(function(data, status, headers, config){                 
                      //$scope.get_product();
                       alert("Product has been Updated Successfully");
                       $scope.transSearch();
                })
                .error(function(data, status, headers, config){
                       
                });
    }
    $scope.delete = function(delTransNo){
         var x = confirm("Are you sure you want to delete the selected transaction?");
         if(x){
          $http.post('model.php?action=delete_product', 
                {
                    'transno'     : delTransNo
                }
            )      
            .success(function (data, status, headers, config) {               
                 alert("Product has been deleted Successfully");
                 $scope.transSearch();
            })

            .error(function(data, status, headers, config){
               
            });
          }else{

          }   
    }
});