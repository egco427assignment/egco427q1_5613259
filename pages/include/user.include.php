<?php

class User{
    private $uname;
    private $pwd;
    private $admin;

    function __construct($username, $password, $adm){
        $this->uname = $username;
        $this->pwd = $password;
        $this->admin = $adm;
    }

    public function getUname(){
        return $this->uname;
    }
    public function getPwd(){
        return $this->pwd;
    }
    public function getAdmin(){
        return $this->admin;
    }
}

?>