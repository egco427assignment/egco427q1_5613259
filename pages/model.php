<?php
include('include/database.php');

/**  Switch Case to Get Action from controller  **/

switch($_GET['action'])  {
    case 'add_product' :
            add_product($conn);
            break;

    case 'get_product' :
            get_product($conn);
            break;

    case 'edit_product' :
            edit_product();
            break;

    case 'delete_product' :              
            delete_product($conn);
            break;

    case 'update_product' :
            update_product($conn);
            break;
}


/**  Function to Add Product  **/

function add_product($conn) {
    $postdata = json_decode(file_get_contents("php://input")); 
    $number      = $postdata->number;
    $date     = $postdata->date;
    $sellerno  = $postdata->sellerno;
    $product = $postdata->product;
    $price = $postdata->price;
    
    // find uid
    $qry = "SELECT * FROM users WHERE username LIKE '%".$_COOKIE['username']."%'";
    $result = mysqli_query($conn,$qry);
    $row = mysqli_fetch_assoc($result);
    $uid = $row['id'];
 
    //print_r($data);
    //$qry = $sql = "INSERT INTO `cardinfo` (`name`, `number`, `issuer`, `exp`, `limit`, `currency`, `uid`) VALUES (\'$name\', \'$number\', \'issuer\', \'$exp\', \'99\', \'\', \'12\')";
    //$qry = "INSERT INTO `cardinfo` (`name`, `number`, `issuer`, `exp`, `limit`, `currency`, `uid`) VALUES (\'laughing\', \'\', \'\', \'\', \'99\', \'\', \'11\')";
    $qry = "INSERT INTO `cardstatement` (`date`, `sellerno`, `product`, `price`, `number`, `uid`, `transno`) VALUES ('".$date."', '".$sellerno."', '".$product."', '".$price."', '".$number."', '".$uid."', NULL)";
    //echo $qry."<br>";
    //$qry="INSERT INTO `cardstatement` (`date`, `sellerno`, `product`, `price`, `number`, `uid`, `transno`) VALUES ('2016-04-02', '222', '222', '222', '222', '222', NULL)";
    //echo $qry."<br>";
    if(isset($number)){
        $qry_res = mysqli_query($conn,$qry);
        if ($qry_res) {
            $arr = array('msg' => "Product Added Successfully!!!", 'error' => '');
            $jsn = json_encode($arr);
             print_r($jsn);
        } 
        else {
            $arr = array('msg' => "", 'error' => 'Error In inserting record');
            $jsn = json_encode($arr);
             print_r($jsn);
        }
    }
}


/**  Function to Get Product  **/

function get_product($conn) {    
    $postData = json_decode(file_get_contents("php://input"));
    $TransNo = $postData->transno;
    //$TransNo = 231;
    //echo "<script type='text/javascript'>alert('$uid');</script>";
    //$qry = mysql_query('SELECT * from cardstate where transno = ' . $TransNo);
    $qry = "SELECT * from cardstatement where transno = " . $TransNo;
    $result = mysqli_query($conn, $qry);
    $data = array();
    while($rows = mysqli_fetch_array($result))
    {
        $data[] = array(
                    "date"            => $rows['date'],
                    "sellerno"     => $rows['sellerno'],
                    "product"     => $rows['product'],
                    "price"    => $rows['price'],
                    "number" => $rows['number'],
                    "uid" => $rows['uid'],
                    "transno" => $rows['transno']
                    );
    }
    print_r(json_encode($data));
    //print_r($uid);
    return json_encode($data);  
    //$data = json_decode(file_get_contents("php://input"));
    //$uid =  $data->uid;
   // $qry = mysql_query('SELECT * from cardinfo where uid = ' . $uid);
}


/**  Function to Delete Product  **/

function delete_product($conn) {
    $data = json_decode(file_get_contents("php://input"));     
    $TransNo = $data->transno;    
    //$TransNo = 231; 
    //print_r($data)   ;
    $qry = "DELETE FROM cardstatement WHERE transno = ".$TransNo;
    //echo $qry;
    $del = mysqli_query($conn, $qry);
    if($del){
        return true;
    }
    return false;     
}


/**  Function to Edit Product  **/

// function edit_product() {
//     $data = json_decode(file_get_contents("php://input"));     
//     $index = $data->prod_index; 
//     $qry = mysql_query('SELECT * from product WHERE id='.$index);
//     $data = array();
//     while($rows = mysql_fetch_array($qry))
//     {
//         $data[] = array(
//                     "id"            =>  $rows['id'],
//                     "prod_name"     =>  $rows['prod_name'],
//                     "prod_desc"     =>  $rows['prod_desc'],
//                     "prod_price"    =>  $rows['prod_price'],
//                     "prod_quantity" =>  $rows['prod_quantity']
//                     );
//     }
//     print_r(json_encode($data));
//     return json_encode($data);  
// }


/** Function to Update Product **/

function update_product($conn) {
    $data = json_decode(file_get_contents("php://input")); 
    $sellerno  = $data->sellerno;
    $product = $data->product;
    $price = $data->price;
    $transno = $data->transno;

    // $sellerno  = 1111;
    // $product = 1111;
    // $price = 2222;
    // $transno = 230;

   // print_r($data);
    
    //$qry = "UPDATE product set prod_name='".$prod_name."' , prod_desc='".$prod_desc."',prod_price='.$prod_price.',prod_quantity='.$prod_quantity.' WHERE id=".$id;
    $qry = "UPDATE `cardstatement` SET `sellerno` = '".$sellerno."', `product` = '".$product."', `price` = '".$price."' WHERE `cardstatement`.`transno` = '".$transno."';";
    echo $qry;
    $qry_res = mysqli_query($conn, $qry);
    if ($qry_res) {
        $arr = array('msg' => "Product Updated Successfully!!!", 'error' => '');
        $jsn = json_encode($arr);
        // print_r($jsn);
    } else {
        $arr = array('msg' => "", 'error' => 'Error In Updating record');
        $jsn = json_encode($arr);
        // print_r($jsn);
    }
}

?>